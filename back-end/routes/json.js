var express = require("express");
var router = express.Router();
var fs = require("fs");

var response;

fs.readFile("../products.json", "utf8", (err, data) => {
  if (err) {
    console.log("File read failed:", err);
    return;
  }
  response = data;
});

router.get("/", function (req, res, next) {
  res.send(response);
});

module.exports = router;
