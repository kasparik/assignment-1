var express = require("express");
var router = express.Router();
var fs = require("fs");
var convert = require("xml-js");

var response;

fs.readFile("../products.xml", "utf8", (err, data) => {
  if (err) {
    console.log("File read failed:", err);
    return;
  }
  try {
    response = convert.xml2json(data, { compact: true, spaces: 4 });
  } catch (err) {
    console.log("Error parsing JSON string:", err);
  }
});

router.get("/", function (req, res, next) {
  res.send(response);
});

module.exports = router;
