var { buildSchema } = require("graphql");

// GraphQL schema
var schema = buildSchema(`
    type Query {
        books(title: String): [Book]
    },
    type Book {
        title: String
        author: String
        cover: String
        description: String
        quantity: String
        price: String
    }
`);

module.exports = schema;
