var axios = require("axios");

const GetProducts = async () => {
  return await axios
    .get("http://localhost:9000/product/json")
    .then(function (response) {
      return response.data.products;
    });
};

module.exports = GetProducts();
