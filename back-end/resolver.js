var data = require("./data");

var getBooks = function (args) {
  if (args.title) {
    var title = args.title;

    return data.then((res) => {
      return res.product.filter((book) => book.title === title);
    });
  } else {
    return data.then((res) => {
      return res.product;
    });
  }
};

var root = {
  books: getBooks,
};

module.exports = root;
