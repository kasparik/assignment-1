# INFO3160 - Assignment 1

**Authors:** Kevin Kaspari & Ryan Beausoleil

## Startup instructions

### Back-end

1. In a terminal, navigate to the `back-end` directory
2. Run `npm install` then `npm start`, the server will run on port `9000`


### Front-end


1. In a terminal, navigate to the front-end directory
2. Run `npm install` then `npm start`, the client will run on port `3000`
3. If the client does not automatically open in your default browser, please navigate to [http://localhost:3000](http://localhost:3000) manually

## How to use

1. Click the buttons on the client to alternate between datasources (JSON and XML). React might make it seem like nothing is happening, but changes can be observed in the console and network tabs of your browser's developer tools.
2. To test GraphQL endpoint, you can navigate to [http://localhost:3000/product/graphql](http://localhost:3000/product/graphql) to use the graphiql interface. Alternatively you can hit that endpoint from Postman or a client of your choice.
3. Sample GraphQL queries at located in the project root

## License
 
The MIT License (MIT)

Copyright (c) 2021 Kevin Kaspari

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
