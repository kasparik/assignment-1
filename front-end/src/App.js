import "./App.css";
import React, { useEffect, useState } from "react";
import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";
import axios from "axios";

function App() {
  const [products, setProducts] = useState([]);

  const GetXML = async () => {
    console.log("In GetXML");

    const result = await axios("http://localhost:9000/product/xml");

    let prodArr = result.data.products.product;

    let rows = [];

    for (let i in prodArr) {
      let book = prodArr[i];
      let styleClass = "";

      if (book.quantity._text === "0") {
        styleClass = "zeroQuantity";
      }

      rows.push(
        <tr key={i} className={styleClass}>
          <td>
            <img
              src={book.cover._text}
              className="img-fluid"
              alt={`Cover for ${book.title._text}`}
            />
          </td>
          <td>{book.title._text}</td>
          <td>{book.author._text}</td>
          <td>{book.description._text}</td>
          <td>{book.price._text}</td>
          <td>{book.quantity._text}</td>
        </tr>
      );
    }

    setProducts(rows);
  };

  const GetJSON = async () => {
    console.log("In GetJSON");

    const result = await axios("http://localhost:9000/product/json");
    let prodArr = result.data.products.product;

    let rows = [];

    for (let i in prodArr) {
      let book = prodArr[i];
      let styleClass = "";

      if (book.quantity === "0") {
        styleClass = "zeroQuantity";
      }

      rows.push(
        <tr key={i} className={styleClass}>
          <td>
            <img
              src={book.cover}
              className="img-fluid"
              alt={`Cover for ${book.title}`}
            />
          </td>
          <td>{book.title}</td>
          <td>{book.author}</td>
          <td>{book.description}</td>
          <td>{book.price}</td>
          <td>{book.quantity}</td>
        </tr>
      );
    }

    setProducts(rows);
  };

  return (
    <div className="App">
      <h1>INFO3160 Assignment 1</h1>
      <p>Select a source for the data:</p>
      <Button
        onClick={() => {
          GetJSON();
        }}
      >
        JSON
      </Button>{" "}
      <Button
        onClick={() => {
          GetXML();
        }}
      >
        XML
      </Button>
      <Table className="prodTable" striped bordered>
        <thead>
          <tr>
            <th>Cover</th>
            <th>Title</th>
            <th>Author</th>
            <th>Description</th>
            <th>Price</th>
            <th>Quantity</th>
          </tr>
        </thead>
        <tbody>{products}</tbody>
      </Table>
    </div>
  );
}

export default App;
